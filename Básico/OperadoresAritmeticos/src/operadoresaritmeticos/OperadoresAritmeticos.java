package operadoresaritmeticos;

public class OperadoresAritmeticos {

    public static void main(String[] args) {
        /* int n1 = 3;
        int n2 = 5;
        float media = (n1 + n2) / 2;
        System.out.println("A média é igual a: " + media); */
        
        // Operador Unário
        /* int numero = 1;
        numero++;
        System.out.println(numero); */
        
        int numero = 5;
        // Nesse caso soma primeiro e depois adiciona um ao número
        int valor1 = 5 + numero++;
        // Soma e já adiciona um ao número
        int valor2 = 5 + ++numero;
        // O mesmo se aplica ao decremento
        int valor3 = 5 + numero--;
        int valor4 = 5 + --numero;
       
        
        // Operadores de Atribuição
        int x = 4;
        x += 2; // x = x + 2;
        
        // Clase Math
        float v = 8.3f;
        int arBaixo = (int) Math.floor(v);
        int arCima = (int) Math.ceil(v);
        int arComum = (int) Math.round(v);
        
        double aleaotorio = Math.random();
    }
    
}
