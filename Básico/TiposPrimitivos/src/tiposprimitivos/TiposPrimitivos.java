package tiposprimitivos;

import java.util.Scanner;

public class TiposPrimitivos {

    public static void main(String[] args) {
        // Saída de dados
        System.out.println("--------------|Saída de dados|--------------");
        String nome = "Willian";
        float nota = 8.5f;
        // Com Pulo de linha
        System.out.println("Sua nota é:" + nota);
        // Sem pulo de linha
        System.out.print("Sua nota é:" + nota);
        
        System.out.println("\n");
        // Formatado
        // Depois do ponto é quantidade de casas decimais
        System.out.printf("A nota de %s é: %.2f \n", nome, nota);
        // printf pode ser substituido por: 
        System.out.format("A nota de %s é: %.2f \n", nome, nota);
        
        
        System.out.println("--------------|Entrada de Dados|--------------");
        // Entrada de Dados
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Digite o nome do aluno:");
        String nome2 = scanner.nextLine();
        System.out.println("Digite a nota do aluno:");
        Double nota2 = scanner.nextDouble();
        System.out.format("A nota de %s é: %.2f \n", nome2, nota2);
        
        System.out.println("--------------|Conversão de Tipos|--------------");
        
    }
    
}
